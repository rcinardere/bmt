package com.cinardere.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * 
 * @author Ghuraba
 * 
 *         This entity gives detail information about the amount in the invoice
 *
 */
@Entity
public class Amount extends AbstractBMTEntity {

	@Column
	private Double netto;

	@Column
	private Double salesTax;

	@Column
	private Double brutto;

	@Column
	private Double sum;

	public Amount() {
	}

	public Double getNetto() {
		return netto;
	}

	public void setNetto(Double netto) {
		this.netto = netto;
	}

	public Double getSalesTax() {
		return salesTax;
	}

	public void setSalesTax(Double salesTax) {
		this.salesTax = salesTax;
	}

	public Double getBrutto() {
		return brutto;
	}

	public void setBrutto(Double brutto) {
		this.brutto = brutto;
	}

	public Double getSum() {
		return sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}

}
