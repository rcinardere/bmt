package com.cinardere.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * 
 * @author Ghuraba
 * 
 *         This entity gives information about a project
 *
 */
@Entity
public class Project extends AbstractBMTEntity {

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE }, optional = false)
	private Provider provider;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE }, optional = false)
	private Recruiter recruiter;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE }, optional = false)
	private Customer customer;

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	private ProjectParameter parameter;

	@ElementCollection
	@CollectionTable(name = "TASK", joinColumns = @JoinColumn(name = "ID"))
	@Column(name = "TASK_DESCRIPTION")
	private List<String> tasks = new ArrayList<>();

	@Column(name = "CONTRACT")
	private byte[] contract;

	@Column(nullable = false)
	private String description;

	@Column(unique = true, nullable = false)
	private String projectId;

	public Project() {
		// TODO Auto-generated constructor stub
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public Recruiter getRecruiter() {
		return recruiter;
	}

	public void setRecruiter(Recruiter recruiter) {
		this.recruiter = recruiter;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public byte[] getContract() {
		return contract;
	}

	public void setContract(byte[] contract) {
		this.contract = contract;
	}

	public ProjectParameter getParameter() {
		return parameter;
	}

	public void setParameter(ProjectParameter parameter) {
		this.parameter = parameter;
	}

	public List<String> getTasks() {
		return tasks;
	}

	public void setTasks(List<String> tasks) {
		this.tasks = tasks;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
