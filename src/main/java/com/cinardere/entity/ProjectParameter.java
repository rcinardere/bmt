package com.cinardere.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * 
 * @author Ghuraba
 *
 *         This entity gives meta-information about project-parameters
 */
@Entity
public class ProjectParameter extends AbstractBMTEntity {

	@Column
	private Date begin;

	@Column
	private Date finish;

	@Column
	private Double fee;

	@Column
	private Integer dayOfPayment;

	@Column
	private Date signedAt;

	public ProjectParameter() {
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Integer getDayOfPayment() {
		return dayOfPayment;
	}

	public void setDayOfPayment(Integer dayOfPayment) {
		this.dayOfPayment = dayOfPayment;
	}

	public Date getSignedAt() {
		return signedAt;
	}

	public void setSignedAt(Date signedAt) {
		this.signedAt = signedAt;
	}

	public Date getFinish() {
		return finish;
	}

	public void setFinish(Date finish) {
		this.finish = finish;
	}
	
	

}
