package com.cinardere.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * 
 * @author Ghuraba
 * 
 *         Entity for invoices
 */
@Entity
public class Invoice extends AbstractBMTEntity {

	@OneToOne
	private Project project;

	@OneToOne
	private Effort task;

	@Column
	private Date date;

	@Column
	private String invoiceNumber;

	@Column
	private String subject;

	@Column
	private String salutation;

	@Column
	private String closing;

	public Invoice() {
		// TODO Auto-generated constructor stub
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getClosing() {
		return closing;
	}

	public void setClosing(String closing) {
		this.closing = closing;
	}

	public Effort getTask() {
		return task;
	}

	public void setTask(Effort task) {
		this.task = task;
	}

}
