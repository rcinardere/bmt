package com.cinardere.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * 
 * @author Ghuraba
 * 
 *         Abstract Entity generates unique for each subclass-entity
 *
 */

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractBMTEntity {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Long created;

	@Column
	private Long updated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@PrePersist
	public void prePersist() {
		this.created = System.currentTimeMillis();
	}

	@PreUpdate
	public void preUpdate() {
		this.updated = System.currentTimeMillis();
	}

	protected Long getUpdated() {
		return updated;
	}

	protected Long getCreated() {
		return created;
	}

}
