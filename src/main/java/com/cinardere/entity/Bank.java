package com.cinardere.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * 
 * @author Ghuraba
 *
 *         This entity gives information about the bank-account
 */
@Entity
public class Bank extends AbstractBMTEntity {

	@Column
	private String name;

	@Column
	private String iban;

	@Column
	private String bic;

	public Bank() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

}
