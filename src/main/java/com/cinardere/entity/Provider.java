package com.cinardere.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * 
 * @author Ghuraba
 * 
 *         This entity gives information about a provider
 */

@Entity
public class Provider extends AbstractPerson {

	@OneToOne(mappedBy = "provider")
	private Project project;

	@OneToOne(cascade = CascadeType.ALL)
	private Bank bank;

	@Column(unique = true, nullable = false)
	private String saleTaxId;

	public Provider() {
		// TODO Auto-generated constructor stub
	}

	public String getSaleTaxId() {
		return saleTaxId;
	}

	public void setSaleTaxId(String saleTaxId) {
		this.saleTaxId = saleTaxId;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

}
