package com.cinardere.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * 
 * @author Ghuraba
 * 
 *         Entity for customer
 */
@Entity
public class Customer extends AbstractBMTEntity {

	@Column(nullable = false, unique = true)
	private String company;

	@Column
	private String name;

	@OneToOne(cascade = CascadeType.ALL)
	private Address address;

	@OneToOne(mappedBy = "customer")
	private Project project;

	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}
