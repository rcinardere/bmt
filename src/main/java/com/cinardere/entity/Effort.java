package com.cinardere.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * 
 * @author Ghuraba
 * 
 *         This entity gives information about the effort of the provider
 *
 */
@Entity
public class Effort extends AbstractBMTEntity {

	@Column
	private Date begin;

	@Column
	private Date finish;

	@Column
	private String description;

	@Column
	private Double time;

	public Effort() {
		// TODO Auto-generated constructor stub
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getFinish() {
		return finish;
	}

	public void setFinish(Date finish) {
		this.finish = finish;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getTime() {
		return time;
	}

	public void setTime(Double time) {
		this.time = time;
	}

}
