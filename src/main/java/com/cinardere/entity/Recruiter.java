package com.cinardere.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * 
 * @author Ghuraba
 * 
 *         This entity gives information about a recruiter
 *
 */
@Entity
public class Recruiter extends AbstractPerson {

	@OneToOne(mappedBy = "recruiter")
	private Project project;

	@Column(nullable = false, unique = true)
	private String company;

	public Recruiter() {
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

}
