package com.cinardere.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.cinardere.enums.Country;

/**
 * 
 * @author Ghuraba
 * 
 *         Entity for address
 *
 */
@Entity
public class Address extends AbstractBMTEntity {

	@Column
	private String street;

	@Column
	private String zipCode;

	@Column
	private String city;

	@Column
	private String no;

	@Enumerated(EnumType.STRING)
	private Country country;

	public Address() {
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

}
