package com.cinardere.model;

/**
 * 
 * @author Ghuraba
 *
 *         This class acts as a value-object. This vo represents the the
 *         salutation of an invoice
 *
 */
public class Salutation {

	private String title;
	private String body;
	private String end;
	private String sincerly;

	public Salutation() {
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getSincerly() {
		return sincerly;
	}

	public void setSincerly(String sincerly) {
		this.sincerly = sincerly;
	}

}
