package com.cinardere.model;

import com.cinardere.service.interfaces.IRemunerationDetails;

/**
 * 
 * @author Ghuraba
 *
 */
public class Remuneration implements IRemunerationDetails {

	private Double netto;

	private Double tax;

	private Double brutto;

	public Remuneration() {
		// TODO Auto-generated constructor stub
	}

	public void setNetto(Double netto) {
		this.netto = netto;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public void setBrutto(Double brutto) {
		this.brutto = brutto;
	}

	@Override
	public Double getNetto() {
		return netto;
	}

	@Override
	public Double getTax() {
		return tax;
	}

	@Override
	public Double getBrutto() {
		return brutto;
	}

}
