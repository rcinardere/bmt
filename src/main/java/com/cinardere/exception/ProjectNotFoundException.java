package com.cinardere.exception;

import java.text.MessageFormat;

/**
 * 
 * @author Ghuraba
 *
 */
public class ProjectNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String ERROR_MSG = "Project with id {0} not exists";

	public ProjectNotFoundException(String projectId) {
		super(MessageFormat.format(ERROR_MSG, projectId));
	}
}
