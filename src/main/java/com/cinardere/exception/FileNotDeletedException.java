package com.cinardere.exception;

import java.io.File;
import java.text.MessageFormat;

public class FileNotDeletedException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String ERROR_MSG = "File with name {0} cannot be deleted";

	public FileNotDeletedException(File file) {
		super(MessageFormat.format(ERROR_MSG, file.getName()));
	}
}
