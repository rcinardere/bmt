package com.cinardere.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import com.cinardere.dto.ProjectDto;
import com.cinardere.util.ProjectIdGenerator;

/**
 * 
 * @author Ghuraba
 * 
 *         This aspect is responsible for manipulating behavior of projects: -
 *         Generates an random UUID as projectId. The generated UUID will be
 *         assigned to a project.
 *
 */
@Aspect
@Component
@Configurable
public class ProjectIDAspect {
	
	@Autowired
	private ProjectIdGenerator generator;

	@Before("execution(* com.cinardere.service.impl.ProjectServiceImpl.save(..))")
	public void beforeSave(JoinPoint joinPoint) {

		if (joinPoint.getArgs()[0] != null && joinPoint.getArgs()[0] instanceof ProjectDto) {
			ProjectDto dto = (ProjectDto) joinPoint.getArgs()[0];
			dto.setProjectId(generator.generateId(dto));
		}

	}

}
