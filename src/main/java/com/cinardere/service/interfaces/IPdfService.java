package com.cinardere.service.interfaces;

import org.springframework.core.io.Resource;

import com.cinardere.entity.Invoice;

/**
 * 
 * @author Ghuraba
 * 
 *         Provides methods for managing pdf-documents
 *
 */
public interface IPdfService {

	/**
	 * 
	 * Generates a PDF-Document which represents a invoice
	 * 
	 * @param invoice
	 * @return
	 * @throws Exception
	 */
	public Resource generatePdf(Invoice invoice) throws Exception;

}
