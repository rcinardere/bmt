package com.cinardere.service.interfaces;

import java.io.IOException;
import java.util.List;

import org.springframework.core.io.Resource;

import com.cinardere.dto.InvoiceDto;
import com.cinardere.entity.Invoice;

/**
 * 
 * @author Ghuraba
 *
 *         This service provides methods for the businessflow of the domain
 *         'invoice'
 *
 */
public interface IInvoiceService {

	/**
	 * This method returns a list of all created invoices
	 * 
	 * @return list of {@link Invoice}
	 */
	public List<Invoice> getAll();

	/**
	 * 
	 * Generates a full-qualified invoice and returns it as a resource
	 * 
	 * @param dto
	 * @return
	 * @throws IOException 
	 */
	public Resource generateInvoice(InvoiceDto dto) throws Exception;

}
