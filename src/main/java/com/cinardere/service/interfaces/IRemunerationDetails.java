package com.cinardere.service.interfaces;

/**
 * 
 * @author Ghuraba
 *
 */
public interface IRemunerationDetails {

	/**
	 * 
	 * @return
	 */
	public Double getNetto();

	/**
	 * 
	 * @return
	 */
	public Double getTax();

	/**
	 * 
	 * @return
	 */
	public Double getBrutto();
}
