package com.cinardere.service.interfaces;

import com.cinardere.entity.Invoice;

/**
 * 
 * @author Ghuraba
 *
 */
public interface ICalculationService {

	/**
	 * 
	 * @param invoice
	 * @return
	 */
	public IRemunerationDetails calculateRemuneration(Invoice invoice);

}
