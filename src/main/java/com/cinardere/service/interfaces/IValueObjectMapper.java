package com.cinardere.service.interfaces;

import com.cinardere.entity.AbstractBMTEntity;

/**
 * 
 * @author Ghuraba
 *
 */
public interface IValueObjectMapper {

	<T extends AbstractBMTEntity> T convertFromFile();
	
	
}
