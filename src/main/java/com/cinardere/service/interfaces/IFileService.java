package com.cinardere.service.interfaces;

import java.io.File;

import com.cinardere.entity.Invoice;

/**
 * 
 * @author Ghuraba
 *
 */
public interface IFileService {

	/**
	 * 
	 * Generates a file which refers to invoice which will be created
	 * 
	 * @param invoice
	 * @return
	 */
	public File createFile(Invoice invoice);

	/**
	 * 
	 * @param folderName
	 * @param fileName
	 * @return
	 */
	public String getContentOfFile(String folderName, String fileName);

}
