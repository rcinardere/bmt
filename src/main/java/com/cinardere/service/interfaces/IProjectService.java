package com.cinardere.service.interfaces;

import java.util.List;

import com.cinardere.dto.ProjectDto;
import com.cinardere.entity.Project;

/**
 * 
 * @author Ghuraba
 *
 *         This interfaces declares methodes for operations which handles
 *         'Project' Entity
 */
public interface IProjectService {

	/**
	 * 
	 * Saves a new project
	 * 
	 * @param dto The dto which should be mapped and persisted
	 * 
	 * @return persisted entity
	 */
	public Project save(ProjectDto dto);

	/**
	 * 
	 * Returns all projects
	 * 
	 * @return
	 */
	public List<Project> getAll();

	/**
	 * Returns a single project by unique identifier @param projectId
	 * 
	 * @param projectId
	 * @return
	 */
	public Project findOneById(String projectId);

	/**
	 * Deletes a project by given @param projectId
	 * 
	 * @param projectId
	 * @return
	 */
	public void deleteById(String projectId);

}
