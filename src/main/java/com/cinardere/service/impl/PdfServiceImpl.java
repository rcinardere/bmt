package com.cinardere.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.cinardere.converter.ISalutationConverter;
import com.cinardere.entity.Address;
import com.cinardere.entity.Bank;
import com.cinardere.entity.Invoice;
import com.cinardere.entity.Provider;
import com.cinardere.entity.Recruiter;
import com.cinardere.enums.BMTFonts;
import com.cinardere.enums.CalculationTable;
import com.cinardere.enums.FooterTable;
import com.cinardere.enums.InvoiceDetailTable;
import com.cinardere.enums.ServiceTable;
import com.cinardere.enums.SpecialCharacters;
import com.cinardere.model.Salutation;
import com.cinardere.service.interfaces.ICalculationService;
import com.cinardere.service.interfaces.IFileService;
import com.cinardere.service.interfaces.IPdfService;
import com.cinardere.service.interfaces.IRemunerationDetails;
import com.cinardere.util.BMTUtils;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

/**
 * 
 * @author Ghuraba
 *
 *         This component creates is repsonsible for creating and filling
 *         pdf-documents.
 *
 */
@Service
public class PdfServiceImpl implements IPdfService {

	@Autowired
	private IFileService fileService;

	@Autowired
	private ISalutationConverter salutationConverter;

	@Autowired
	private ICalculationService calculationService;

	private Paragraph newLine = new Paragraph(new Text("\n"));

	@Override
	public Resource generatePdf(Invoice invoice) throws Exception {

		File file = fileService.createFile(invoice);
		Document document = createDocument(file);

		// Address - Header from Provider
		Paragraph addressHeader = createAddressHeader(invoice.getProject().getProvider());
		document.add(addressHeader);

		// Address from client (recipient, recruiter)
		Paragraph recipient = createAddressRecruiter(invoice.getProject().getRecruiter());
		document.add(recipient);
		document.add(newLine);

		// Invoice details
		Table invoiceDetails = createInvoiceDetails(invoice);
		document.add(invoiceDetails);
		document.add(newLine);

		// Subject
		Paragraph subject = createSubject();
		document.add(subject);
		subject.setMarginBottom(Float.valueOf(8));
		
		// Salutation
		Salutation salutation = createSalutation();
		document.add(new Paragraph(salutation.getTitle()));
		document.add(new Paragraph(salutation.getBody()).setTextAlignment(TextAlignment.JUSTIFIED).setMarginBottom(Float.valueOf(10)));
		document.setBottomMargin(Float.valueOf(10));

		// Service details
		Table serviceDetails = createServiceDetails(invoice);
		document.add(serviceDetails);
		document.add(newLine);
		
		// Calculation
		Table calculation = createCalculation(invoice);
		document.add(calculation);
		document.add(newLine);

		// Salutation - End
		document.add(new Paragraph(salutation.getEnd()).setTextAlignment(TextAlignment.JUSTIFIED).setMarginBottom(Float.valueOf(10)));

		// Salutation - Sincerly
		Paragraph sincerly = new Paragraph(salutation.getSincerly()).setTextAlignment(TextAlignment.JUSTIFIED);
		document.add(sincerly);
		document.add(newLine);

		// Singature - Image
		Image image = createImage();
		image.setFixedPosition(53, 160);
		document.add(image);

		// Signature - Name
		Paragraph signature = createSignature(invoice.getProject().getProvider());
		document.add(signature);
		document.add(newLine);

		// Footer
		Table footer = createFooter(invoice.getProject().getProvider());
		document.add(footer);

		document.close();
		return new UrlResource(file.toURI());
	}

	private Image createImage() {
		Image image = null;
		try {

			final URL url = ResourceUtils.getURL("classpath:images/signature.png");
			final ImageData imageData = ImageDataFactory.create(url);
			image = new Image(imageData);
			image.scaleToFit(Float.valueOf(120), Float.valueOf(160));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return image;
	}

	private Table createFooter(Provider provider) {

		// Fußzeile
		Table table = new Table(4);
		table.setWidth(UnitValue.createPercentValue(100));
		for (int row = 0; row < 5; row++) {
			for (int column = 0; column < 4; column++) {

				Cell cell = new Cell();
				Paragraph paragraph = new Paragraph();
				paragraph.setFontSize(7.5f);

				Address address = provider.getAddress();
				FooterTable type = FooterTable.fromCoordinates(row, column);
				Bank bank = provider.getBank();

				if (type != null) {

					if (type == FooterTable.NAME) {
						paragraph.add(provider.getPreName() + SpecialCharacters.WHITESPACE.getValue()
								+ provider.getSurName());
					}

					else if (type == FooterTable.ADRESS) {
						paragraph.add(address.getStreet() + SpecialCharacters.WHITESPACE.getValue() + address.getNo());
					}

					else if (type == FooterTable.ZIP_CITY) {
						paragraph.add(
								address.getZipCode() + SpecialCharacters.WHITESPACE.getValue() + address.getCity());
					}

					else if (type == FooterTable.PHONE) {
						paragraph.add(provider.getPhone());
					}

					else if (type == FooterTable.EMAIL) {
						paragraph.add(provider.getEmail());
					}

					else if (type.isDescription()) {
						paragraph.add(type.getCellName());
					}

					else if (type == FooterTable.DEPOSITOR_DESCRIPTION_VALUE) {
						paragraph.add(provider.getPreName() + SpecialCharacters.WHITESPACE.getValue()
								+ provider.getSurName());
					}

					else if (type == FooterTable.BANK_VALUE) {
						paragraph.add(bank.getName());
					}

					else if (type == FooterTable.IBAN_VALUE) {
						paragraph.add(bank.getIban());
					}

					else if (type == FooterTable.BIC_VALUE) {
						paragraph.add(bank.getBic());
					}
				}

				cell.setBorder(Border.NO_BORDER);
				cell.add(paragraph);
				table.addCell(cell);
			}

		}

		return table;
	}

	private Paragraph createSignature(Provider provider) {
		final String name = String.join(SpecialCharacters.WHITESPACE.getValue(), provider.getPreName(),
				provider.getSurName());
		return new Paragraph(name);
	}

	private Table createCalculation(Invoice invoice) {

		// Calculation
		Table table = new Table(4);
		table.setWidth(UnitValue.createPercentValue(100));
		IRemunerationDetails remuneration = calculationService.calculateRemuneration(invoice);
		DecimalFormat formatter = new DecimalFormat("##,###.00");

		for (int row = 0; row < 4; row++) {
			for (int column = 0; column < 4; column++) {

				CalculationTable type = CalculationTable.fromCoordinates(row, column);
				Cell cell = new Cell();
				Paragraph paragraph = new Paragraph();

				if (type != null) {

					if (type.isDescription() && type == CalculationTable.SUM_DESCRIPTION) {
						paragraph.setFont(BMTFonts.BOLD.font());
						paragraph.add(type.getCellName());
						cell.setTextAlignment(TextAlignment.LEFT);
					}

					else if (type.isDescription()) {
						paragraph.add(type.getCellName());
						cell.setTextAlignment(TextAlignment.LEFT);
					}

					else if (type == CalculationTable.NETTO_VALUE) {
						paragraph.add(formatter.format(remuneration.getNetto())
								+ SpecialCharacters.WHITESPACE.getValue() + SpecialCharacters.EURO.getValue());
						cell.setTextAlignment(TextAlignment.RIGHT);
					}

					else if (type == CalculationTable.TAX_VALUE) {
						paragraph.add(formatter.format(remuneration.getTax()) + SpecialCharacters.WHITESPACE.getValue()
								+ SpecialCharacters.EURO.getValue());
						cell.setTextAlignment(TextAlignment.RIGHT);
					}

					else if (type == CalculationTable.BRUTTO_VALUE) {
						paragraph.add(formatter.format(remuneration.getBrutto())
								+ SpecialCharacters.WHITESPACE.getValue() + SpecialCharacters.EURO.getValue());
						cell.setTextAlignment(TextAlignment.RIGHT);
					}

					else if (type == CalculationTable.SUM_VALUE) {
						paragraph.setFont(BMTFonts.BOLD.font());
						paragraph.add(formatter.format(remuneration.getBrutto())
								+ SpecialCharacters.WHITESPACE.getValue() + SpecialCharacters.EURO.getValue());
						cell.setTextAlignment(TextAlignment.RIGHT);
					}

				}

				cell.add(paragraph);
				cell.setWidth(UnitValue.createPercentValue(25));
				cell.setBorder(Border.NO_BORDER);
				table.addCell(cell);

			}
		}

		return table;
	}

	private Table createServiceDetails(Invoice invoice) {
		// Service description
		Table table = new Table(4);
		table.setWidth(UnitValue.createPercentValue(100));

		for (int row = 0; row < 4; row++) {
			for (int column = 0; column < 4; column++) {

				ServiceTable serviceTable = ServiceTable.fromCoordinates(row, column);

				if (serviceTable != null) {

					if (serviceTable.isDescription()) {

						Paragraph p = new Paragraph(serviceTable.getCellName()).setFont(BMTFonts.BOLD.font());
						Cell cell = new Cell();
						cell.add(p);
						cell.setTextAlignment(TextAlignment.CENTER);
						table.addHeaderCell(cell);

					}

					else if (serviceTable == ServiceTable.DESCRIPTION_VALUE) {
						Paragraph p = new Paragraph(invoice.getProject().getDescription());
						Cell cell = new Cell();
						cell.add(p);
						cell.setTextAlignment(TextAlignment.CENTER);
						table.addCell(cell);
					}

					else if (serviceTable == ServiceTable.TIME_PERIOD_VALUE) {

						SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
						SimpleDateFormat sdfNoYear = new SimpleDateFormat("dd.MM.");
						
						String von = sdfNoYear.format(invoice.getTask().getBegin());
						String bis = sdf.format(invoice.getTask().getFinish());

						final String text = von + SpecialCharacters.WHITESPACE.getValue()
								+ SpecialCharacters.HYPHEN.getValue() + SpecialCharacters.WHITESPACE.getValue() + bis;
						Paragraph p = new Paragraph(text);
						Cell cell = new Cell();
						cell.add(p);
						cell.setTextAlignment(TextAlignment.CENTER);
						table.addCell(cell);
					}

					else if (serviceTable == ServiceTable.EFFORT_VALUE) {
						final String time = String.valueOf(invoice.getTask().getTime())
								+ SpecialCharacters.WHITESPACE.getValue() + SpecialCharacters.HOURS.getValue();
						Paragraph p = new Paragraph(time);
						Cell cell = new Cell();
						cell.add(p);
						cell.setTextAlignment(TextAlignment.CENTER);
						table.addCell(cell);
					}

					else if (serviceTable == ServiceTable.HONORAR_VALUE) {
						final String text = String.valueOf(invoice.getProject().getParameter().getFee())
								+ SpecialCharacters.WHITESPACE.getValue() + SpecialCharacters.EURO.getValue()
								+ SpecialCharacters.WHITESPACE.getValue() + SpecialCharacters.BACKSLASH.getValue()
								+ SpecialCharacters.HOURS.getValue();

						Paragraph p = new Paragraph(text);
						Cell cell = new Cell();
						cell.add(p);
						cell.setTextAlignment(TextAlignment.CENTER);
						table.addCell(cell);
					}

				}

			}
		}

		return table;
	}

	// TODO:
	// Exception Handling
	private Salutation createSalutation() {
		return salutationConverter.convertFromJson(BMTUtils.FILENAME_SALUTATION_JSON);
	}

	private Paragraph createSubject() {

		Text subject = new Text(BMTUtils.TXT_SUBJECT);
		subject.setFontSize(16);
		return new Paragraph(subject).setFont(BMTFonts.BOLD.font());

	}

	private Table createInvoiceDetails(Invoice invoice) {

		// Service description
		Table table = new Table(4);
		table.setWidth(UnitValue.createPercentValue(100));

		// Invoice Details
		// Fill Table
		for (int row = 0; row < 3; row++) {
			for (int column = 0; column < 4; column++) {

				Cell cell = new Cell();
				Paragraph paragraph = new Paragraph();

				InvoiceDetailTable invoiceDetails = InvoiceDetailTable.fromCoordinates(row, column);

				if (invoiceDetails != null) {

					if (invoiceDetails.isDescription()) {
						Text t = new Text(invoiceDetails.getCellName());

						paragraph.add(t);
						cell.add(paragraph);
						cell.setWidth(UnitValue.createPercentValue(25));
					}

					if (invoiceDetails == InvoiceDetailTable.DATE_VALUE) {
						Text t2 = new Text(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
						paragraph.add(t2);
						cell.add(paragraph);
						cell.setTextAlignment(TextAlignment.RIGHT);
						cell.setWidth(UnitValue.createPercentValue(25));
					}

					else if (invoiceDetails == InvoiceDetailTable.TAX_ID_VALUE) {
						Text t2 = new Text(invoice.getProject().getProvider().getSaleTaxId());
						paragraph.add(t2);
						cell.add(paragraph);
						cell.setTextAlignment(TextAlignment.RIGHT);
						cell.setWidth(UnitValue.createPercentValue(25));
					}

					else if (invoiceDetails == InvoiceDetailTable.INVOICE_NO_VALUE) {
						Text t2 = new Text(invoice.getInvoiceNumber());
						paragraph.add(t2);
						cell.add(paragraph);
						cell.setTextAlignment(TextAlignment.RIGHT);
						cell.setWidth(UnitValue.createPercentValue(25));
					}

				}

				cell.setBorder(Border.NO_BORDER);
				table.addCell(cell);
			}
		}
		return table;

	}

	private Document createDocument(File file) throws IOException {

		final PdfFont arial = PdfFontFactory.createFont(BMTFonts.ARIAL.getPath(), PdfEncodings.WINANSI, true);
		PdfWriter writer = new PdfWriter(file.getName());
		PdfDocument pdf = new PdfDocument(writer);

		Document document = new Document(pdf, PageSize.A4);
		document.setFont(arial);
		document.setFontSize(10f);
		document.setMargins(80, 80, 40, 70);
		return document;

	}

	private Paragraph createAddressHeader(Provider provider) {

		final Address address = provider.getAddress();

		final String name = provider.getPreName() + SpecialCharacters.WHITESPACE.getValue() + provider.getSurName();
		final String streetNo = String.join(SpecialCharacters.WHITESPACE.getValue(), address.getStreet(),
				address.getNo());
		final String zipCity = String.join(SpecialCharacters.WHITESPACE.getValue(), address.getZipCode(),
				address.getCity());

		final String chained = String.join(
				SpecialCharacters.PIPE_OPERATOR.getValue() + SpecialCharacters.WHITESPACE.getValue(), name, streetNo,
				zipCity);

		final Text text = new Text(chained);
		text.setFontSize(8);
		text.setUnderline();
		return new Paragraph(text);
	}

	private Paragraph createAddressRecruiter(Recruiter recruiter) {

		final Address address = recruiter.getAddress();

		// Client
		final String streetNo = String.join(SpecialCharacters.WHITESPACE.getValue(), address.getStreet(),
				String.valueOf(address.getNo()));
		final String zipCity = String.join(SpecialCharacters.WHITESPACE.getValue(), address.getZipCode(),
				address.getCity());

		final String chained = String.join(SpecialCharacters.NEW_LINE.getValue(), recruiter.getCompany(), streetNo,
				zipCity);

		return new Paragraph(chained);
	}

}
