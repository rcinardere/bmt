package com.cinardere.service.impl;

import java.text.MessageFormat;
import java.time.Month;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.cinardere.dto.InvoiceDto;
import com.cinardere.entity.Invoice;
import com.cinardere.mapper.IInvoiceMapper;
import com.cinardere.repository.IInvoiceRepository;
import com.cinardere.service.interfaces.IInvoiceService;
import com.cinardere.service.interfaces.IPdfService;
import com.cinardere.util.BMTUtils;

/**
 * 
 * @author Ghuraba
 *
 *         Business-Workflow-Service-Layer for the domain 'Invoice'
 */
@Service
public class InvoiceServiceImpl implements IInvoiceService {

	@Autowired
	private IInvoiceRepository repo;

	@Autowired
	private IPdfService pdfService;

	@Autowired
	private IInvoiceMapper mapper;

	@Override
	public List<Invoice> getAll() {
		return repo.findAll();
	}

	@Override
	public Resource generateInvoice(InvoiceDto dto) throws Exception {
		
		Invoice invoice = mapper.fromDtoToPojo(dto);

		assignInvoiceNumber(invoice);

		return pdfService.generatePdf(invoice);
	}

	private void assignInvoiceNumber(Invoice invoice) {

		final String upperLetter = invoice.getProject().getRecruiter().getCompany().substring(0, 2).toUpperCase();
		final Calendar cal = Calendar.getInstance();

		final Month month = Month.values()[cal.get(Calendar.MONTH)];
		final String invoiceNumber = MessageFormat.format(BMTUtils.INVOICE_NO_PATTERN, 
				upperLetter,
				String.valueOf(cal.get(Calendar.YEAR)), 
				month.getValue());

		invoice.setInvoiceNumber(invoiceNumber);
	}

}
