package com.cinardere.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cinardere.dto.ProjectDto;
import com.cinardere.entity.Project;
import com.cinardere.mapper.IProjectMapper;
import com.cinardere.repository.IProjectRepository;
import com.cinardere.service.interfaces.IProjectService;

/**
 * 
 * @author Ghuraba
 *
 */
@Service
public class ProjectServiceImpl implements IProjectService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectServiceImpl.class);

	@Autowired
	private IProjectRepository repo;

	@Autowired
	private IProjectMapper mapper;

	@Override
	public Project save(ProjectDto dto) {
		LOGGER.info("Try to save new project");
		Project fromDb = repo.findByProjectId(dto.getProjectId());

		if (fromDb == null) {
			Project pojo = mapper.fromDtoToPojo(dto);
			fromDb = repo.save(pojo);
		}

		return fromDb;

	}

	@Override
	public List<Project> getAll() {
		return repo.findAll();
	}

	@Override
	public Project findOneById(String projectId) {
		return getOneById(projectId);
	}

	@Override
	public void deleteById(String projectId) {
		LOGGER.info("Try to delete project with id: " + projectId);
		Project fromDb = getOneById(projectId);
		repo.delete(fromDb);
	}

	private Project getOneById(String projectId) {
		Project fromDb = repo.findByProjectId(projectId);
		if (fromDb == null) {
			// TODO:
			// Throw Exception
		}

		return fromDb;
	}
}
