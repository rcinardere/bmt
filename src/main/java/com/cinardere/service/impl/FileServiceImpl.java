package com.cinardere.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.Month;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.cinardere.entity.Invoice;
import com.cinardere.service.interfaces.IFileService;
import com.cinardere.util.BMTUtils;

/**
 * 
 * @author Ghuraba
 *
 *         Responsible for managing files
 */
@Service
public class FileServiceImpl implements IFileService {

	@Override
	public File createFile(Invoice invoice) {

		final String lettersOfCompany = invoice.getProject().getCustomer().getCompany().substring(0, 2).toUpperCase();
		final Calendar cal = new GregorianCalendar();
		cal.setTime(invoice.getDate());

		final Month month = Month.values()[cal.get(Calendar.MONTH)];
		final String fileName = MessageFormat.format(BMTUtils.FILENAME_PATTERN, lettersOfCompany,
				String.valueOf(cal.get(Calendar.YEAR)), month.getValue());

		return new File(fileName);

	}

	// TODO:
	// Exception handling
	@Override
	public String getContentOfFile(String folderName, String fileName) {

		String content = null;

		try {
			Path path = Paths.get(getClass().getClassLoader().getResource(folderName + "/" + fileName).toURI());
			content = Files.readAllLines(path).stream().collect(Collectors.joining());
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}

		return content;
	}

}
