package com.cinardere.service.impl;

import org.springframework.stereotype.Service;

import com.cinardere.entity.Invoice;
import com.cinardere.model.Remuneration;
import com.cinardere.service.interfaces.ICalculationService;
import com.cinardere.service.interfaces.IRemunerationDetails;
import com.cinardere.util.BMTUtils;

/**
 * 
 * @author Ghuraba
 *
 */
@Service
public class CalculationServiceImpl implements ICalculationService {

	@Override
	public IRemunerationDetails calculateRemuneration(Invoice invoice) {

		final Double fee = invoice.getProject().getParameter().getFee();
		final Remuneration remuneration = new Remuneration();

		Double netto = invoice.getTask().getTime() * fee;
		remuneration.setNetto(netto);

		Double tax = netto * BMTUtils.TAX_VALUE / 100;
		remuneration.setTax(tax);

		Double brutto = netto + tax;
		remuneration.setBrutto(brutto);

		return remuneration;
	}

}
