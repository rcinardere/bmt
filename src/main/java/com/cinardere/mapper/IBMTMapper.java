package com.cinardere.mapper;

/**
 * 
 * Provides method for mapping
 * 
 * @author Ghuraba
 *
 */
public interface IBMTMapper {

	/**
	 * 
	 * This method generates a new instance of the @param entity. Afterwards all
	 * primitive and java-standard property-values from the @param source will be
	 * mapped to the same named properties of the currently created object
	 * 
	 * @param obj
	 * @param clazz
	 * @return
	 */
	public <E> E map(Object source, Class<E> classOfEntity);

}
