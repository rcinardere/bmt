package com.cinardere.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinardere.dto.CustomerDto;
import com.cinardere.dto.ProjectDto;
import com.cinardere.dto.ProjectParameterDto;
import com.cinardere.dto.ProviderDto;
import com.cinardere.dto.RecruiterDto;
import com.cinardere.entity.Customer;
import com.cinardere.entity.Project;
import com.cinardere.entity.ProjectParameter;
import com.cinardere.entity.Provider;
import com.cinardere.entity.Recruiter;

/**
 * 
 * @author Ghuraba
 * 
 *         Responsible for mapping from pojos and data-transfer-objects
 *
 */
@Component
public class ProjectMapperImpl implements IProjectMapper {

	@Autowired
	private IBMTMapper mapper;

	@Override
	public Project fromDtoToPojo(ProjectDto dto) {

		Project pojo = mapper.map(dto, Project.class);
			pojo.setCustomer(mapper.map(dto.getCustomer(), Customer.class));
			pojo.setParameter(mapper.map(dto.getParameter(), ProjectParameter.class));
			pojo.setProvider(mapper.map(dto.getProvider(), Provider.class));
			pojo.setRecruiter(mapper.map(dto.getRecruiter(), Recruiter.class));
		return pojo;
	}

	@Override
	public ProjectDto fromPojoToDto(Project pojo) {

		ProjectDto dto = mapper.map(pojo, ProjectDto.class);
			dto.setCustomer(mapper.map(pojo.getCustomer(), CustomerDto.class));
			dto.setParameter(mapper.map(pojo.getParameter(), ProjectParameterDto.class));
			dto.setProvider(mapper.map(pojo.getProvider(), ProviderDto.class));
			dto.setRecruiter(mapper.map(pojo.getRecruiter(), RecruiterDto.class));
		return dto;
	}

	@Override
	public List<ProjectDto> fromPojoToDto(List<Project> pojos) {
		return pojos.stream()
				.map(pojo -> fromPojoToDto(pojo))
				.collect(Collectors.toList());
	}

}
