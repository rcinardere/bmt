package com.cinardere.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cinardere.dto.EffortDto;
import com.cinardere.dto.InvoiceDto;
import com.cinardere.entity.Effort;
import com.cinardere.entity.Invoice;
import com.cinardere.entity.Project;
import com.cinardere.service.interfaces.IProjectService;

/**
 * 
 * @author Ghuraba
 *
 */
@Service
public class InvoiceMapperImpl implements IInvoiceMapper {

	@Autowired
	private IBMTMapper mapper;

	@Autowired
	private IProjectMapper projectMapper;
	
	@Autowired
	private IProjectService projectService;

	@Override
	public Invoice fromDtoToPojo(InvoiceDto dto) {

		if (dto == null) {
			throw new IllegalArgumentException("Parameter (dto) should not be null");
		}

		Invoice pojo = mapper.map(dto, Invoice.class);
		Project project = projectService.findOneById(dto.getProject().getProjectId());
		Effort task = mapper.map(dto.getTask(), Effort.class);
			pojo.setProject(project);
			pojo.setTask(task);
		return pojo;
	}

	@Override
	public InvoiceDto fromPojoToDto(Invoice pojo) {

		if (pojo == null) {
			throw new IllegalArgumentException("Parameter (pojo) should not be null");
		}

		InvoiceDto dto = new InvoiceDto();
			dto.setDate(pojo.getDate());
			dto.setProject(projectMapper.fromPojoToDto(pojo.getProject()));
			dto.setTask(mapper.map(pojo.getTask(), EffortDto.class));
		return dto;
	}

}
