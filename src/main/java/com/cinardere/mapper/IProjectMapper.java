package com.cinardere.mapper;

import java.util.List;

import com.cinardere.dto.ProjectDto;
import com.cinardere.entity.Project;

/**
 * 
 * @author Ghuraba
 * 
 *         Provides methods for converting data in bidirectional way: 
 *         - POJO -> DTO 
 *         - DTO  -> POJO
 *
 */
public interface IProjectMapper {

	/**
	 * 
	 * Converts a dto to an pojo object
	 * 
	 * @param dto
	 * @return
	 */
	public Project fromDtoToPojo(ProjectDto dto);

	/**
	 * 
	 * Converts a pojo object to a dto object
	 * 
	 * @param pojo
	 * @return
	 */
	public ProjectDto fromPojoToDto(Project pojo);

	/**
	 * 
	 * Converts a list of pojos into a list of dtos
	 * 
	 * @param findAll
	 * @return
	 */
	public List<ProjectDto> fromPojoToDto(List<Project> findAll);
}
