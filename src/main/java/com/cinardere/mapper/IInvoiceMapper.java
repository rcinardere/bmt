package com.cinardere.mapper;

import com.cinardere.dto.InvoiceDto;
import com.cinardere.entity.Invoice;

/**
 * 
 * @author Ghuraba
 * 
 *         Provides methods for converting data in bidirectional way: - POJO ->
 *         DTO - DTO -> POJO
 *
 */
public interface IInvoiceMapper {

	/**
	 * 
	 * Converts a dto to pojo
	 * 
	 * @param dto
	 * @return
	 */
	public Invoice fromDtoToPojo(InvoiceDto dto);

	/**
	 * 
	 * Converts a pojo to dto
	 * 
	 * @param pojo
	 * @return
	 */
	public InvoiceDto fromPojoToDto(Invoice pojo);

}
