package com.cinardere.mapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cinardere.dto.AddressDto;
import com.cinardere.dto.BankDto;
import com.cinardere.entity.Address;
import com.cinardere.entity.Bank;
import com.cinardere.enums.Country;

/**
 * 
 * @author Ghuraba
 *
 */
@Component
public class BMTMapperImpl implements IBMTMapper {

	@Override
	public <E> E map(Object source, Class<E> classOfEntity) {
		E newInstance = null;

		if (source == null) {
			return newInstance;
		}

		try {
			newInstance = classOfEntity.getConstructor().newInstance();

			for (Method m : source.getClass().getMethods()) {

				if (!m.getDeclaringClass().equals(Object.class) && isReturnTypePrimitive(m)
						&& !m.getReturnType().isAssignableFrom(void.class)) {

					final String methodName = m.getName();

					if (!methodName.contains("Class")) {
						final Object value = m.invoke(source);
						Method setter = getSetter(newInstance, methodName);
						setter.invoke(newInstance, value);
					}
				}

				if (m.getReturnType().isAssignableFrom(Address.class)
						|| m.getReturnType().isAssignableFrom(AddressDto.class)
						|| m.getReturnType().isAssignableFrom(Bank.class)
						|| m.getReturnType().isAssignableFrom(BankDto.class)) {
					final String nameOfGetter = m.getName();
					final Object value = m.invoke(source);

					Class<?> retType = getReturnType(newInstance, m);
					Object obj = map(value, retType);
					Method setter = getSetter(newInstance, nameOfGetter);
					setter.invoke(newInstance, obj);
				}

			}

		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		return newInstance;
	}

	private boolean isReturnTypePrimitive(Method method) {
		return method.getReturnType().isAssignableFrom(String.class)
				|| method.getReturnType().isAssignableFrom(Number.class)
				|| method.getReturnType().isAssignableFrom(Double.class)
				|| method.getReturnType().isAssignableFrom(Date.class)
				|| method.getReturnType().isAssignableFrom(Integer.class)
				|| method.getReturnType().isAssignableFrom(byte[].class)
				|| method.getReturnType().isAssignableFrom(Country.class)
				|| method.getReturnType().isAssignableFrom(List.class);
	}

	private Method getSetter(Object obj, String methodName) {

		final String name = methodName.substring(3, methodName.length());

		for (Method m : obj.getClass().getMethods()) {
			if (m.getName().startsWith("set") && m.getName().contains(name)) {
				return m;
			}
		}
		throw new IllegalArgumentException("No setter found for method " + name);
	}

	private <E> Class<?> getReturnType(E entity, Method method) {

		for (Method m : entity.getClass().getMethods()) {
			if (m.getName().equals(method.getName())) {
				return m.getReturnType();
			}
		}

		return null;

	}

}
