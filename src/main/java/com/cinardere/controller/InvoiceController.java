package com.cinardere.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cinardere.dto.InvoiceDto;
import com.cinardere.entity.Invoice;
import com.cinardere.service.interfaces.IInvoiceService;

/**
 * 
 * @author Ghuraba
 * 
 *         This REST-API provides Http-Methods for enable CRUD-Operations for
 *         the domain Invoice
 *
 */
@RestController
@RequestMapping(value = "/invoice")
public class InvoiceController {

	@Autowired
	private IInvoiceService service;

	@GetMapping
	public ResponseEntity<List<Invoice>> getAll() {
		return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
	}

	@PostMapping("/new")
	public ResponseEntity<Resource> createInvoice(@RequestBody InvoiceDto dto) throws Exception {

		Resource file = service.generateInvoice(dto);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);

	}

}
