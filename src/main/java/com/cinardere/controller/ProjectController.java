package com.cinardere.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cinardere.dto.ProjectDto;
import com.cinardere.mapper.IProjectMapper;
import com.cinardere.service.interfaces.IProjectService;

/**
 * 
 * @author Ghuraba
 * 
 *         REST-API defines REST-Methods for managing of projects
 *
 */
@RestController
@RequestMapping("/project")
public class ProjectController {

	@Autowired
	private IProjectService service;

	@Autowired
	private IProjectMapper mapper;

	/**
	 * 
	 * Post a new project. During the workflow a new project will be created und
	 * returned with an unique identifier.
	 * 
	 * @param dto
	 * @return Created project
	 */
	@PostMapping
	public ResponseEntity<ProjectDto> postProject(@RequestBody ProjectDto dto) {

		ProjectDto created = mapper.fromPojoToDto(service.save(dto));

		return new ResponseEntity<ProjectDto>(created, HttpStatus.CREATED);
	}

	/**
	 * Returns a collection of all available projects as dto
	 * 
	 * @param page
	 * @return A list of all available projects
	 */
	@GetMapping(path = "/projects")
	public ResponseEntity<List<ProjectDto>> getAll() {
		return new ResponseEntity<List<ProjectDto>>(mapper.fromPojoToDto(service.getAll()), HttpStatus.OK);
	}

	/**
	 * Returns a project by unique identifier
	 * 
	 * @param page
	 * @return A list of all available projects
	 */
	@GetMapping(path = "/{projectId}")
	public ResponseEntity<ProjectDto> getOne(@PathVariable String projectId) {
		return new ResponseEntity<ProjectDto>(mapper.fromPojoToDto(service.findOneById(projectId)), HttpStatus.OK);
	}

	/**
	 * Deletes a project by unique identifier
	 * 
	 * @param page
	 * @return A list of all available projects
	 */
	@DeleteMapping(path = "/{projectId}")
	public ResponseEntity<Void> deleteOne(@PathVariable String projectId) {
		service.deleteById(projectId);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
