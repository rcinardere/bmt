package com.cinardere.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.cinardere.entity.Provider;

@Repository
public interface IProviderRepository extends PagingAndSortingRepository<Provider, Long>{

}
