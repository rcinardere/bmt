package com.cinardere.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.cinardere.entity.Customer;

/**
 * 
 * @author Ghuraba
 *
 *         JPA-Repository for the entity 'Client'
 */
@Repository
public interface ICustomerRepository extends PagingAndSortingRepository<Customer, Long> {

}
