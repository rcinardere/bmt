package com.cinardere.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.cinardere.entity.Project;

/**
 * 
 * @author Ghuraba
 *
 *         JPA-Repository for the entity 'Project'
 */
@Repository
public interface IProjectRepository extends PagingAndSortingRepository<Project, Long> {

	/**
	 * Return a list of all project-entities
	 */
	public List<Project> findAll();

	/**
	 * Return a single project-entity by projectId
	 * 
	 * @param projectId
	 * @return
	 */
	public Project findByProjectId(String projectId);

}
