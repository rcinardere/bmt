package com.cinardere.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.cinardere.entity.Bank;

@Repository
public interface IBankRepository extends PagingAndSortingRepository<Bank, Long> {

}
