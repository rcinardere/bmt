package com.cinardere.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.cinardere.entity.Invoice;

/**
 * 
 * @author Ghuraba
 * 
 *         JPA-Repository for the entity Invoice
 *
 */
public interface IInvoiceRepository extends PagingAndSortingRepository<Invoice, Long> {

	/**
	 * Returns a list of all available Invoices
	 */
	List<Invoice> findAll();
}
