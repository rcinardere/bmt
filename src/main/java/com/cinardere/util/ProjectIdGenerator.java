package com.cinardere.util;

import java.nio.charset.StandardCharsets;

import org.springframework.stereotype.Component;

import com.cinardere.dto.ProjectDto;
import com.google.common.hash.Hashing;

/**
 * 
 * @author Ghuraba
 * 
 *         Responsible for generating of hash
 */
@Component
public class ProjectIdGenerator {

	/**
	 * 
	 * This methods returns a hashed string which is represendted as unique
	 * projectId
	 * 
	 * @return
	 */
	public String generateId(ProjectDto projectDto) {

		final String uniqueIdentifier = projectDto.getDescription() 
				+ projectDto.getProvider().getEmail()
				+ projectDto.getProvider().getSaleTaxId()
				+ projectDto.getCustomer().getCompany() 
				+ projectDto.getRecruiter().getCompany();
		
		return Hashing.sha1().hashString(uniqueIdentifier, StandardCharsets.UTF_8).toString();
	}

}
