package com.cinardere.util;

/**
 * 
 * @author Ghuraba
 * 
 *         Utility class
 *
 */
public class BMTUtils {

	/**
	 * 0 => The first second letters of the customers company name 1 => Year of
	 * creation of invoice 2 => Month of cration of invoice
	 */
	public static final String FILENAME_PATTERN = "{0}_{1}_{2}.pdf";

	public static final String INVOICE_NO_PATTERN = "{0}_{1}_{2}";

	public static final String TXT_SUBJECT = "Rechnung";

	public static final String FOLDER_TEXT = "text";

	public static final String FILENAME_SALUTATION_JSON = "salutation.json";

	public static final String NETTO = "Netto ";

	public static final Double TAX_VALUE = 19.0;

}
