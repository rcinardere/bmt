package com.cinardere.converter;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cinardere.model.Salutation;
import com.cinardere.service.interfaces.IFileService;
import com.cinardere.util.BMTUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Ghuraba
 *
 */
@Service
public class SalutationConverterImpl implements ISalutationConverter {

	@Autowired
	private IFileService fileService;

	@Autowired
	private ObjectMapper mapper;

	@Override
	public Salutation convertFromJson(final String fileName) {

		final String data = fileService.getContentOfFile(BMTUtils.FOLDER_TEXT, fileName);
		try {
			return mapper.readValue(data, Salutation.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}
