package com.cinardere.converter;

import com.cinardere.model.Salutation;

/**
 * 
 * @author Ghuraba
 *
 */
public interface ISalutationConverter {

	/**
	 * 
	 * @param json
	 * @return
	 */
	public Salutation convertFromJson(String fileName);

}
