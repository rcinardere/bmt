package com.cinardere.enums;

/**
 * 
 * @author Ghuraba
 *
 */
public enum SpecialCharacters {

	WHITESPACE(" "), PIPE_OPERATOR("|"), NEW_LINE("\n"), HYPHEN("-"), EURO("€"), HOURS("h"), BACKSLASH("/");

	private final String value;

	private SpecialCharacters(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
