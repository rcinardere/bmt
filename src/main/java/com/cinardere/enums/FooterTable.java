package com.cinardere.enums;

import java.util.stream.Stream;

/**
 * 
 * @author Ghuraba
 *
 */
public enum FooterTable {

	NAME(0, 0, null), 
	ADRESS(1, 0, null), 
	ZIP_CITY(2, 0, null), 
	PHONE(3, 0, null), 
	EMAIL(4, 0, null),
	
	DEPOSITOR_DESCRIPTION(0, 2, "Kontoinhaber"), DEPOSITOR_DESCRIPTION_VALUE(0, 3, null),
	
	BANK_DESCRIPTION(1, 2, "Bankfiliale"), BANK_VALUE(1, 3, null), 
	
	IBAN_DESCRIPTION(2, 2, "IBAN"), IBAN_VALUE(2, 3, null), 
	
	BIC_DESCRIPTION(3, 2, "BIC"), BIC_VALUE(3, 3, null);

	final int row;
	final int column;
	final String cellName;

	private FooterTable(int row, int column, String cellName) {
		this.row = row;
		this.column = column;
		this.cellName = cellName;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	public String getCellName() {
		return cellName;
	}

	public static FooterTable fromCoordinates(int row, int column) {
		return Stream.of(values())
				.filter(type -> type.getRow() == row && type.getColumn() == column)
				.findFirst()
				.orElse(null);
	}

	public boolean isDescription() {
		return this.getCellName() != null;
	}

}
