package com.cinardere.enums;

import java.util.stream.Stream;

/**
 * 
 * @author Ghuraba
 *
 */
public enum InvoiceDetailTable {

	DATE_DESCRIPTION(0, 2, "Rechnungsdatum:"), DATE_VALUE(0, 3, null),

	TAX_ID_DESCRIPTION(1, 2, "USt-IdNr.:"), TAX_ID_VALUE(1, 3, null),

	INVOICE_NO_DESCRIPTION(2, 2, "Rechnungs-Nr.:"), INVOICE_NO_VALUE(2, 3, null);

	final int row;
	final int column;
	final String cellName;

	private InvoiceDetailTable(int row, int column, String cellName) {
		this.row = row;
		this.column = column;
		this.cellName = cellName;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}
	
	public String getCellName() {
		return cellName;
	}

	public static InvoiceDetailTable fromCoordinates(int row, int column) {

		return Stream.of(values())
				.filter(type -> type.getRow() == row && type.getColumn() == column)
				.findFirst()
				.orElse(null);
	}
	
	public boolean isDescription() {
		return this.getCellName() != null;
	}
	

}
