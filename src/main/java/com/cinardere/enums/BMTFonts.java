package com.cinardere.enums;

import java.io.IOException;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;

/**
 * 
 * @author Ghuraba
 *
 */
public enum BMTFonts {

	ARIAL("fonts/Arial.ttf") {
		@Override
		public PdfFont font() {

			try {
				return PdfFontFactory.createFont(getPath(), PdfEncodings.WINANSI, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	},
	BOLD("") {
		@Override
		public PdfFont font() {
			try {
				return PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	};

	private final String path;
	
	private BMTFonts(final String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}
	
	public abstract PdfFont font();
}
