package com.cinardere.enums;

import java.util.stream.Stream;

/**
 * 
 * @author Ghuraba
 *
 */
public enum ServiceTable {

	DESCRIPTION_DESCRIPTION(0, 0, "Beschreibung"), DESCRIPTION_VALUE(1, 0, null),

	TIME_PERIOD_DESCRIPTION(0, 1, "Zeitraum"), TIME_PERIOD_VALUE(1, 1, null),

	EFFORT_DESCRIPTION(0, 2, "Leistung in Stunden"), EFFORT_VALUE(1, 2, null),

	HONORAR_DESCRIPTION(0, 3, "Honorar"), HONORAR_VALUE(1, 3, null);

	final int row;
	final int column;
	final String cellName;

	private ServiceTable(int row, int column, String cellName) {
		this.row = row;
		this.column = column;
		this.cellName = cellName;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	public String getCellName() {
		return cellName;
	}
	
	public boolean isDescription() {
		return this.getCellName() != null;
	}

	public static ServiceTable fromCoordinates(int row, int column) {
		return Stream.of(values())
				.filter(type -> type.getRow() == row && type.getColumn() == column)
				.findFirst()
				.orElse(null);
	}

}
