package com.cinardere.enums;

import java.util.stream.Stream;

/**
 * 
 * @author Ghuraba
 *
 */
public enum CalculationTable {

	NETTO_DESCRIPTION(0, 2, "Netto"), NETTO_VALUE(0, 3, null),

	TAX_DESCRIPTION(1, 2, "+ MwSt. Anteil (19%)"), TAX_VALUE(1, 3, null),

	BRUTTO_DESCRIPTION(2, 2, "= Brutto (inkl. MwSt.)"), BRUTTO_VALUE(2, 3, null),

	SUM_DESCRIPTION(3, 2, "Endbetrag"), SUM_VALUE(3, 3, null);

	final int row;
	final int column;
	final String cellName;

	private CalculationTable(int row, int column, String cellName) {
		this.row = row;
		this.column = column;
		this.cellName = cellName;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	public String getCellName() {
		return cellName;
	}
	
	public boolean isDescription() {
		return this.getCellName() != null;
	}

	public static CalculationTable fromCoordinates(int row, int column) {
		return Stream.of(values())
				.filter(type -> type.getRow() == row && type.getColumn() == column)
				.findFirst()
				.orElse(null);
	}

	
}
