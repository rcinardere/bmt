package com.cinardere.dto;

import java.util.Date;

/**
 * 
 * @author Ghuraba
 *
 *         Data-transfer-object for the entity 'Effort'
 */
public class EffortDto {

	private Date begin;

	private Date finish;

	private String description;

	private Double time;

	public EffortDto() {
		// TODO Auto-generated constructor stub
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getFinish() {
		return finish;
	}

	public void setFinish(Date finish) {
		this.finish = finish;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getTime() {
		return time;
	}

	public void setTime(Double time) {
		this.time = time;
	}

}
