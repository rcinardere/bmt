package com.cinardere.dto;

/**
 * 
 * @author Ghuraba
 * 
 *         DTO for the entity 'Recruiter'
 *
 */
public class RecruiterDto {

	private AddressDto address;

	private String company;

	private String preName;

	private String surName;

	private String phone;

	private String email;

	public RecruiterDto() {
		// TODO Auto-generated constructor stub
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPreName() {
		return preName;
	}

	public void setPreName(String preName) {
		this.preName = preName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
