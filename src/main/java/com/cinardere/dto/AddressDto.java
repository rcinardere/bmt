package com.cinardere.dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.cinardere.enums.Country;

/**
 * 
 * @author Ghuraba
 *
 *         DTO for the entity 'Address'
 */
public class AddressDto {

	private String zipCode;

	private String city;

	private String street;

	private String no;

	@Enumerated(EnumType.STRING)
	private Country country;

	public AddressDto() {
		// TODO Auto-generated constructor stub
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}
