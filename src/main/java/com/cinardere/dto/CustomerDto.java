package com.cinardere.dto;

/**
 * 
 * @author Ghuraba
 *
 *         DTO for the entity 'Customer'
 *
 */
public class CustomerDto {

	private String company;

	private String name;

	private AddressDto address;

	public CustomerDto() {
		// TODO Auto-generated constructor stub
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}

}
