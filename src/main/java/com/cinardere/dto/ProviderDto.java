package com.cinardere.dto;

/**
 * 
 * @author Ghuraba
 *
 *         DTO for the entity 'Provider'
 */
public class ProviderDto {

	private String preName;

	private String surName;

	private String phone;

	private String email;

	private String saleTaxId;

	private AddressDto address;

	private BankDto bank;

	public ProviderDto() {
	}

	public String getPreName() {
		return preName;
	}

	public void setPreName(String preName) {
		this.preName = preName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSaleTaxId() {
		return saleTaxId;
	}

	public void setSaleTaxId(String saleTaxId) {
		this.saleTaxId = saleTaxId;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}

	public BankDto getBank() {
		return bank;
	}

	public void setBank(BankDto bank) {
		this.bank = bank;
	}

}
