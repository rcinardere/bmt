package com.cinardere.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 
 * @author Ghuraba
 * 
 *         Data-transfer-object for the entity 'project'
 *
 */
public class ProjectDto {

	private ProviderDto provider;

	private RecruiterDto recruiter;

	private CustomerDto customer;

	private ProjectParameterDto parameter;

	private List<String> tasks = new ArrayList<>();

	private String projectId;

	private String description;

	private byte[] contract;

	public ProjectDto() {
		// TODO Auto-generated constructor stub
	}

	public ProviderDto getProvider() {
		return provider;
	}

	public void setProvider(ProviderDto provider) {
		this.provider = provider;
	}

	public RecruiterDto getRecruiter() {
		return recruiter;
	}

	public void setRecruiter(RecruiterDto recruiter) {
		this.recruiter = recruiter;
	}

	public CustomerDto getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDto customer) {
		this.customer = customer;
	}

	public ProjectParameterDto getParameter() {
		return parameter;
	}

	public void setParameter(ProjectParameterDto parameter) {
		this.parameter = parameter;
	}

	public List<String> getTasks() {
		return tasks;
	}

	public void setTasks(List<String> tasks) {
		this.tasks = tasks;
	}

	public byte[] getContract() {
		return contract;
	}

	public void setContract(byte[] contract) {
		this.contract = contract;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
