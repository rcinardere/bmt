package com.cinardere.dto;

import java.util.Date;

/**
 * 
 * @author Ghuraba
 * 
 *         This DTO is a representation of the entity 'ProjectParameter'
 */
public class ProjectParameterDto {

	private Date begin;

	private Date finish;

	private Double fee;

	private Integer dayOfPayment;

	private Date signedAt;

	public ProjectParameterDto() {
		// TODO Auto-generated constructor stub
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getFinish() {
		return finish;
	}

	public void setFinish(Date finish) {
		this.finish = finish;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Integer getDayOfPayment() {
		return dayOfPayment;
	}

	public void setDayOfPayment(Integer dayOfPayment) {
		this.dayOfPayment = dayOfPayment;
	}

	public Date getSignedAt() {
		return signedAt;
	}

	public void setSignedAt(Date signedAt) {
		this.signedAt = signedAt;
	}

}
