package com.cinardere.dto;

import java.util.Date;

/**
 * 
 * @author Ghuraba
 *
 *         Date-transfer-object for the entity 'Invoice'
 */
public class InvoiceDto {

	private ProjectDto project;

	private EffortDto task;

	private Date date;

	public InvoiceDto() {
		// TODO Auto-generated constructor stub
	}

	public ProjectDto getProject() {
		return project;
	}

	public void setProject(ProjectDto project) {
		this.project = project;
	}

	public EffortDto getTask() {
		return task;
	}

	public void setTask(EffortDto task) {
		this.task = task;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
