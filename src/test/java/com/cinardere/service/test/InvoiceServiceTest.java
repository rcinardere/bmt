package com.cinardere.service.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Month;
import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileCopyUtils;

import com.cinardere.Application;
import com.cinardere.dto.InvoiceDto;
import com.cinardere.entity.Invoice;
import com.cinardere.entity.Project;
import com.cinardere.exception.FileNotDeletedException;
import com.cinardere.mapper.IProjectMapper;
import com.cinardere.mapper.InvoiceMapperImpl;
import com.cinardere.service.interfaces.IInvoiceService;
import com.cinardere.service.interfaces.IProjectService;
import com.cinardere.utils.BMTTestUtils;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.canvas.parser.PdfTextExtractor;

/**
 * 
 * @author Ghuraba
 *
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = Application.class)
public class InvoiceServiceTest {

	@Autowired
	private IInvoiceService invoiceService;

	@Autowired
	private InvoiceMapperImpl invoiceMapper;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IProjectMapper projectMapper;

	@Test
	public void testCreateInvoice_fileNameCorrectly() throws Exception {

		InvoiceDto dto = createDto();
		Resource resource = invoiceService.generateInvoice(dto);
		Assert.assertNotNull("Resource should not be null nor empty", resource);

		final String fileName = resource.getFilename();
		Assert.assertEquals("Filename of resource should be RE_2018_11.pdf", "RE_2018_11.pdf", fileName);

		deleteFile(resource.getFile());
	}

	
	@Test
	public void testCreateInvoice_addressHeaderCorrectly() throws Exception {

		Invoice invoice = BMTTestUtils.createAdvancedInvoiceForNovember();
		Project project = invoice.getProject();
		Project persisted = projectService.save(projectMapper.fromPojoToDto(project));

		InvoiceDto dto = createDto();
		dto.getProject().setProjectId(persisted.getProjectId());
		Resource resource = invoiceService.generateInvoice(dto);

		PdfReader reader = new PdfReader(resource.getFile());
		PdfDocument pdfDoc = new PdfDocument(reader);
		String content = PdfTextExtractor.getTextFromPage(pdfDoc.getFirstPage());

		int numberOfPage = pdfDoc.getNumberOfPages();
		Assert.assertEquals("Number of pages should be", 1, numberOfPage);

		int noOfLines = content.split("\n").length;
		Assert.assertEquals("Lines of pdf should be 35", 35, noOfLines);

		pdfDoc.close();
		
		
		String directory = System.getProperty("user.home") +"/Documents/Rechnungen/2019/";
		FileCopyUtils.copy(resource.getFile(), new File(directory + resource.getFilename()));
		
//		deleteFile(resource.getFile());
	}

	private void deleteFile(File file) throws FileNotDeletedException {
		if (!file.delete()) {
			throw new FileNotDeletedException(file);
		}
	}

	private InvoiceDto createDto() {
		return invoiceMapper.fromPojoToDto(BMTTestUtils.createAdvancedInvoiceForNovember());
	}

}
