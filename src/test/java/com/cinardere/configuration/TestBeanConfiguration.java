package com.cinardere.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.cinardere.converter.ISalutationConverter;
import com.cinardere.converter.SalutationConverterImpl;
import com.cinardere.mapper.BMTMapperImpl;
import com.cinardere.mapper.IBMTMapper;
import com.cinardere.mapper.IInvoiceMapper;
import com.cinardere.mapper.IProjectMapper;
import com.cinardere.mapper.InvoiceMapperImpl;
import com.cinardere.mapper.ProjectMapperImpl;
import com.cinardere.service.impl.CalculationServiceImpl;
import com.cinardere.service.impl.FileServiceImpl;
import com.cinardere.service.impl.InvoiceServiceImpl;
import com.cinardere.service.impl.PdfServiceImpl;
import com.cinardere.service.impl.ProjectServiceImpl;
import com.cinardere.service.interfaces.ICalculationService;
import com.cinardere.service.interfaces.IFileService;
import com.cinardere.service.interfaces.IInvoiceService;
import com.cinardere.service.interfaces.IPdfService;
import com.cinardere.service.interfaces.IProjectService;
import com.cinardere.util.ProjectIdGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Ghuraba
 *
 */
@Profile("test")
@Configuration
public class TestBeanConfiguration {

	@Bean
	public IProjectMapper projectMapper() {
		return new ProjectMapperImpl();
	}

	@Bean
	public IBMTMapper bmtMapper() {
		return new BMTMapperImpl();
	}

	@Bean
	public ProjectIdGenerator generator() {
		return new ProjectIdGenerator();
	}

	@Bean
	public IInvoiceService invoiceService() {
		return new InvoiceServiceImpl();
	}

	@Bean
	public IPdfService pdfService() {
		return new PdfServiceImpl();
	}

	@Bean
	public IFileService fileService() {
		return new FileServiceImpl();
	}

	@Bean
	public ISalutationConverter converter() {
		return new SalutationConverterImpl();
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

	@Bean
	public ICalculationService calculationService() {
		return new CalculationServiceImpl();
	}

	@Bean
	public IInvoiceMapper invoiceMapper() {
		return new InvoiceMapperImpl();
	}

	@Bean
	public IProjectService projectService() {
		return new ProjectServiceImpl();
	}

}
