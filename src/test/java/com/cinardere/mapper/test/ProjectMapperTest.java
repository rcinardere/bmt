package com.cinardere.mapper.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.cinardere.configuration.TestBeanConfiguration;
import com.cinardere.dto.AddressDto;
import com.cinardere.dto.CustomerDto;
import com.cinardere.dto.ProjectDto;
import com.cinardere.dto.ProviderDto;
import com.cinardere.dto.RecruiterDto;
import com.cinardere.entity.Address;
import com.cinardere.entity.Customer;
import com.cinardere.entity.Project;
import com.cinardere.entity.Provider;
import com.cinardere.entity.Recruiter;
import com.cinardere.mapper.IProjectMapper;
import com.cinardere.utils.BMTTestUtils;

/**
 * 
 * @author Ghuraba
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestBeanConfiguration.class)
public class ProjectMapperTest {

	@Autowired
	private IProjectMapper mapper;

	@Test
	public void testMapFromDtoToPojo_shouldSuccess() {

		ProjectDto dto = BMTTestUtils.createAdvancedDto();
		Project pojo = mapper.fromDtoToPojo(dto);

		Assert.assertNotNull(pojo);
		Assert.assertEquals(dto.getDescription(), pojo.getDescription());
		Assert.assertEquals(dto.getProjectId(), pojo.getProjectId());

		dto.getTasks().stream().forEach(task -> {
			Assert.assertTrue(pojo.getTasks().contains(task));
		});

		assertProvider(dto.getProvider(), pojo.getProvider());
		assertRecruiter(dto.getRecruiter(), pojo.getRecruiter());
		assertCustomer(dto.getCustomer(), pojo.getCustomer());
	}

	@Test
	public void testMapFromPojoToDto_shouldSuccess() {

		Project pojo = BMTTestUtils.createAdvancedPojo();
		ProjectDto dto = mapper.fromPojoToDto(pojo);

		Assert.assertNotNull(pojo);
		Assert.assertEquals(pojo.getDescription(), pojo.getDescription());
		Assert.assertEquals(pojo.getProjectId(), pojo.getProjectId());

		pojo.getTasks().stream().forEach(task -> {
			Assert.assertTrue(pojo.getTasks().contains(task));
		});

		assertProvider(dto.getProvider(), pojo.getProvider());
		assertRecruiter(dto.getRecruiter(), pojo.getRecruiter());
		assertCustomer(dto.getCustomer(), pojo.getCustomer());
	}

	private void assertCustomer(CustomerDto dto, Customer pojo) {
		Assert.assertEquals(dto.getCompany(), pojo.getCompany());
		Assert.assertEquals(dto.getName(), pojo.getName());
		assertAddress(dto.getAddress(), pojo.getAddress());
	}

	private void assertProvider(ProviderDto dto, Provider pojo) {
		Assert.assertEquals(dto.getEmail(), pojo.getEmail());
		Assert.assertEquals(dto.getPhone(), pojo.getPhone());
		Assert.assertEquals(dto.getPreName(), pojo.getPreName());
		Assert.assertEquals(dto.getSaleTaxId(), pojo.getSaleTaxId());
		Assert.assertEquals(dto.getSurName(), pojo.getSurName());
		assertAddress(dto.getAddress(), pojo.getAddress());
	}

	private void assertRecruiter(RecruiterDto dto, Recruiter pojo) {
		Assert.assertEquals(dto.getEmail(), pojo.getEmail());
		Assert.assertEquals(dto.getPhone(), pojo.getPhone());
		Assert.assertEquals(dto.getPreName(), pojo.getPreName());
		Assert.assertEquals(dto.getSurName(), pojo.getSurName());
		Assert.assertEquals(dto.getCompany(), pojo.getCompany());
		assertAddress(dto.getAddress(), pojo.getAddress());
	}

	private void assertAddress(AddressDto dto, Address pojo) {
		Assert.assertEquals(dto.getCity(), pojo.getCity());
		Assert.assertEquals(dto.getStreet(), pojo.getStreet());
		Assert.assertEquals(dto.getZipCode(), pojo.getZipCode());
		Assert.assertEquals(dto.getCountry(), pojo.getCountry());
		Assert.assertEquals(dto.getNo(), pojo.getNo());
	}

}
