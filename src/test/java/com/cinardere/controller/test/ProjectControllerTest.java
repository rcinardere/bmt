package com.cinardere.controller.test;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cinardere.utils.BMTTestUtils;

/**
 * 
 * @author Ghuraba
 *
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class ProjectControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	@Ignore
	public void testPostProject() throws URISyntaxException, Exception {

		String json = BMTTestUtils.getContentOfFile("projectDto.json");

		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.post(new URI("/"))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(json))
				.andReturn();
		
		Assert.assertEquals(201, result.getResponse().getStatus());
	}

}
