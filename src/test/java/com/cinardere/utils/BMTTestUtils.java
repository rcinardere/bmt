package com.cinardere.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.cinardere.dto.AddressDto;
import com.cinardere.dto.CustomerDto;
import com.cinardere.dto.ProjectDto;
import com.cinardere.dto.ProjectParameterDto;
import com.cinardere.dto.ProviderDto;
import com.cinardere.dto.RecruiterDto;
import com.cinardere.entity.Address;
import com.cinardere.entity.Bank;
import com.cinardere.entity.Customer;
import com.cinardere.entity.Effort;
import com.cinardere.entity.Invoice;
import com.cinardere.entity.Project;
import com.cinardere.entity.ProjectParameter;
import com.cinardere.entity.Provider;
import com.cinardere.entity.Recruiter;
import com.cinardere.enums.Country;

/**
 * 
 * @author Ghuraba
 * 
 *         Provides util methods for the test environment
 *
 */
public class BMTTestUtils {

	private static List<String> tasks = Arrays.asList("Implementing of REST-API", "Consulting of Process optimization",
			"Java developing", "Database administration", "Monitoring");

	/**
	 * 
	 * Creates a AddressDto object
	 * 
	 * @param zipCode
	 * @param city
	 * @param street
	 * @param no
	 * @return
	 */
	public static AddressDto createAddress(String zipCode, String city, String street, String no) {
		AddressDto dto = new AddressDto();
		dto.setCountry(Country.GERMANY);
		dto.setZipCode(zipCode);
		dto.setCity(city);
		dto.setStreet(street);
		dto.setNo(no);
		return dto;
	}

	/**
	 * 
	 * Creates a Date object
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 */
	public static Date createDate(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		return cal.getTime();
	}

	/**
	 * 
	 * Create a RecruiterDto object
	 * 
	 * @param address
	 * @param company
	 * @param name
	 * @param email
	 * @param phone
	 * @return
	 */
	public static RecruiterDto createRecruiter(AddressDto address, String company, String surName, String preName,
			String email, String phone) {
		RecruiterDto dto = new RecruiterDto();
		dto.setAddress(address);
		dto.setCompany(company);
		dto.setSurName(surName);
		dto.setPreName(preName);
		dto.setEmail(email);
		dto.setPhone(phone);
		return dto;
	}

	/**
	 * 
	 * Creates a ProviderDto object
	 * 
	 * @param address
	 * @param preName
	 * @param surName
	 * @param saleTaxId
	 * @return
	 */
	public static ProviderDto createProvider(AddressDto address, String preName, String surName, String saleTaxId,
			String phone, String email) {
		ProviderDto dto = new ProviderDto();
		dto.setAddress(address);
		dto.setPreName(preName);
		dto.setSurName(surName);
		dto.setPhone(phone);
		dto.setEmail(email);
		dto.setSaleTaxId(saleTaxId);

		return dto;
	}

	/**
	 * 
	 * Creates a CustomerDto object
	 * 
	 * @param address
	 * @param company
	 * @param name
	 * @return
	 */
	public static CustomerDto createCustomer(AddressDto address, String company, String name) {
		CustomerDto dto = new CustomerDto();
		dto.setAddress(address);
		dto.setCompany(company);
		dto.setName(name);
		return dto;
	}

	/**
	 * 
	 * Creates a ProjectParameterDto object
	 * 
	 * @param begin
	 * @param finish
	 * @param dayOfPayment
	 * @param fee
	 * @param signedAt
	 * @return
	 */
	public static ProjectParameterDto createParameterDto(Date begin, Date finish, int dayOfPayment, Double fee,
			Date signedAt) {
		ProjectParameterDto dto = new ProjectParameterDto();
		dto.setBegin(begin);
		dto.setFinish(finish);
		dto.setFee(fee);
		dto.setDayOfPayment(dayOfPayment);
		dto.setSignedAt(signedAt);
		return dto;
	}

	/**
	 * 
	 * Creates a ProjectDto object
	 * 
	 * @param customer
	 * @param parameter
	 * @param projectId
	 * @param provider
	 * @param recruiter
	 * @param tasks
	 * @return
	 */
	public static ProjectDto createProjectDto(CustomerDto customer, ProjectParameterDto parameter, ProviderDto provider,
			RecruiterDto recruiter, List<String> tasks, String description) {

		ProjectDto dto = new ProjectDto();
		dto.setContract(new byte[] {});
		dto.setCustomer(customer);
		dto.setParameter(parameter);
		dto.setProvider(provider);
		dto.setRecruiter(recruiter);
		dto.setTasks(tasks);
		dto.setDescription(description);
		return dto;
	}

	/**
	 * 
	 * Creates a Address object
	 * 
	 * @param zipCode
	 * @param city
	 * @param street
	 * @param no
	 * @return
	 */
	public static Address createAddressPojo(String zipCode, String city, String street, String no) {
		Address pojo = new Address();
		pojo.setCountry(Country.GERMANY);
		pojo.setZipCode(zipCode);
		pojo.setCity(city);
		pojo.setStreet(street);
		pojo.setNo(no);
		return pojo;
	}

	/**
	 * 
	 * Create a Recruiter object
	 * 
	 * @param address
	 * @param company
	 * @param name
	 * @param email
	 * @param phone
	 * @return
	 */
	public static Recruiter createRecruiterPojo(Address address, String company, String surName, String preName,
			String email, String phone) {
		Recruiter pojo = new Recruiter();
		pojo.setAddress(address);
		pojo.setCompany(company);
		pojo.setSurName(surName);
		pojo.setPreName(preName);
		pojo.setEmail(email);
		pojo.setPhone(phone);
		return pojo;
	}

	/**
	 * 
	 * Creates a Provider object
	 * 
	 * @param address
	 * @param preName
	 * @param surName
	 * @param saleTaxId
	 * @return
	 */
	public static Provider createProviderPojo(Address address, String preName, String surName, String saleTaxId,
			String phone, String email) {
		Provider pojo = new Provider();
		pojo.setAddress(address);
		pojo.setPreName(preName);
		pojo.setSurName(surName);
		pojo.setPhone(phone);
		pojo.setEmail(email);
		pojo.setSaleTaxId(saleTaxId);

		return pojo;
	}

	/**
	 * 
	 * Creates a Customer object
	 * 
	 * @param address
	 * @param company
	 * @param name
	 * @return
	 */
	public static Customer createCustomerPojo(Address address, String company, String name) {
		Customer pojo = new Customer();
		pojo.setAddress(address);
		pojo.setCompany(company);
		pojo.setName(name);
		return pojo;
	}

	/**
	 * 
	 * Creates a ProjectParameter object
	 * 
	 * @param begin
	 * @param finish
	 * @param dayOfPayment
	 * @param fee
	 * @param signedAt
	 * @return
	 */
	public static ProjectParameter createParameterPojo(Date begin, Date finish, int dayOfPayment, Double fee,
			Date signedAt) {
		ProjectParameter pojo = new ProjectParameter();
		pojo.setBegin(begin);
		pojo.setFinish(finish);
		pojo.setFee(fee);
		pojo.setDayOfPayment(dayOfPayment);
		pojo.setSignedAt(signedAt);
		return pojo;
	}

	/**
	 * 
	 * Creates a Project object
	 * 
	 * @param customer
	 * @param parameter
	 * @param projectId
	 * @param provider
	 * @param recruiter
	 * @param tasks
	 * @return
	 */
	public static Project createProjectPojo(Customer customer, ProjectParameter parameter, Provider provider,
			Recruiter recruiter, List<String> tasks, String description) {

		Project pojo = new Project();
		pojo.setContract(new byte[] {});
		pojo.setCustomer(customer);
		pojo.setParameter(parameter);
		pojo.setProvider(provider);
		pojo.setRecruiter(recruiter);
		pojo.setTasks(tasks);
		pojo.setDescription(description);
		return pojo;
	}

	/**
	 * 
	 * Creates a full functional dto
	 * 
	 * @return
	 */
	public static ProjectDto createAdvancedDto() {

		Date begin = createDate(2018, Calendar.JANUARY, 1);
		Date finish = createDate(2018, Calendar.DECEMBER, 31);
		Date signedAt = createDate(2017, Calendar.NOVEMBER, 27);
		int dayOfPayment = 28;
		Double fee = 63.0;

		ProjectParameterDto parameter = createParameterDto(begin, finish, dayOfPayment, fee, signedAt);

		CustomerDto customer = createCustomer(createAddress("90450", "Nürnberg", "Am Südwestpark", "15"), "Bundesagentur",
				"Arbeitsvermittlung");
		ProviderDto provider = createProvider(createAddress("55120", "Mainz", "Kreuzstr. ", "87"), "Ramazan", "Cinardere",
				"UST-667799A", "0162-6666877", "ramazanc86@hotmail.com");

		RecruiterDto recruiter = createRecruiter(createAddress("88888", "München", "Regensburgerstr.", "14"),
				"Freelance Experts", "Max", "Mustermann", "muster@mann.de", "0911-123456789");

		return createProjectDto(customer, parameter, provider, recruiter, tasks, "Portal Entwicklung mit REST");
	}

	/**
	 * Reads the whole content from a file into a string
	 * 
	 * @return
	 * @throws IOException
	 */
	public static String getContentOfFile(final String fileName) throws IOException {
		return new String(Files.readAllBytes(Paths.get("src", "test", "resources", fileName)));

	}

	/**
	 * Creates a advanced Pojo
	 * 
	 * @return
	 */
	public static Project createAdvancedPojo() {
		Date begin = createDate(2018, Calendar.JANUARY, 1);
		Date finish = createDate(2018, Calendar.DECEMBER, 31);
		Date signedAt = createDate(2017, Calendar.NOVEMBER, 27);
		int dayOfPayment = 28;
		Double fee = 63.0;

		ProjectParameter parameter = createParameterPojo(begin, finish, dayOfPayment, fee, signedAt);

		Customer customer = createCustomerPojo(createAddressPojo("90450", "Nürnberg", "Am Südwestpark", "15"),
				"Bundesagentur", "Arbeitsvermittlung");
		Provider provider = createProviderPojo(createAddressPojo("55120", "Mainz", "Kreuzstr. ", "87"), "Ramazan",
				"Cinardere", "UST-667799A", "0162-6666877", "ramazanc86@hotmail.com");

		Recruiter recruiter = createRecruiterPojo(createAddressPojo("88888", "München", "Regensburgerstr.", "14"),
				"Freelance Experts", "Max", "Mustermann", "muster@mann.de", "0911-123456789");

		return createProjectPojo(customer, parameter, provider, recruiter, tasks, "Portal Entwicklung mit REST");
	}

	/**
	 * 
	 * Creates a simple Project-Dto
	 * 
	 * @param i
	 * @return
	 */
	public static ProjectDto createSimpleProjectDto(int i) {
		ProjectDto dto = new ProjectDto();
		dto.setDescription("Project-No: " + i);

		ProviderDto provider = new ProviderDto();
		provider.setSaleTaxId("Ust-ID:01" + i);

		RecruiterDto recruiter = new RecruiterDto();
		recruiter.setCompany("RecruiterCompany-No: " + i);

		CustomerDto customer = new CustomerDto();
		customer.setCompany("Customer-No:" + i);

		dto.setProvider(provider);
		dto.setRecruiter(recruiter);
		dto.setCustomer(customer);
		dto.setParameter(new ProjectParameterDto());
		return dto;
	}

	/**
	 * Creates a advanced pojo of type {@link Invoice}
	 * 
	 * @return
	 */
	public static Invoice createAdvancedInvoiceForNovember() {

		Invoice invoice = new Invoice();
		invoice.setDate(new Date());
		invoice.setInvoiceNumber("Junit-08-2018");

		// Project
		Project project = new Project();
		project.setDescription("Beratung und Entwicklung von Softwareanwendungen");

		// Recruiter
		Recruiter recruiter = new Recruiter();
		recruiter.setCompany("QualityMinds GmbH");
		Address addressRecutier = new Address();
			addressRecutier.setStreet("Ulmenstr");
			addressRecutier.setNo("52a");
			addressRecutier.setCity("Nürnberg");
			addressRecutier.setZipCode("90443");
		recruiter.setAddress(addressRecutier);

		// Projectparameter
		ProjectParameter parameter = new ProjectParameter();
		parameter.setFee(75.0);

		// Effort
		Effort task = new Effort();
		Calendar begin = new GregorianCalendar(2019, Calendar.MARCH, 1);
		Calendar finish = new GregorianCalendar(2019, Calendar.MARCH, 31);

		task.setBegin(begin.getTime());
		task.setFinish(finish.getTime());
		task.setTime(112.0);

		// Provider
		Provider provider = new Provider();
		provider.setPreName("Ramazan");
		provider.setSurName("Cinardere");
		provider.setSaleTaxId("DE307447917");
		provider.setPhone("0162 855 43 45");
		provider.setEmail("ramazanc86@hotmail.com");

		// Address
		Address address = new Address();
			address.setStreet("Kreuzstr.");
			address.setNo("87");
			address.setCity("Mainz");
			address.setZipCode("55120");
		
			
		
		// Bank
		Bank bank = new Bank();
		bank.setName("Sparkasse Mainz");
		bank.setIban("DE50 5505 0120 0200 0990 59");
		bank.setBic("MALADE51MNZ");

		// Assemble
		provider.setAddress(address);
		provider.setBank(bank);

		// Customer
		Customer customer = new Customer();
		customer.setCompany("Recruiter Company");

		project.setProvider(provider);
		project.setParameter(parameter);
		project.setRecruiter(recruiter);
		project.setCustomer(customer);

		invoice.setTask(task);
		invoice.setProject(project);
		return invoice;
	}

}
